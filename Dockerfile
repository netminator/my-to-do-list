FROM alpine
# Copy python requirements file
COPY requirements.txt /tmp/requirements.txt

RUN apk add --update --no-cache g++ gcc libxslt-dev \
bash \
build-base \
curl \
cyrus-sasl-dev \
git \
go \
jpeg-dev \
libc-dev \
libffi-dev \
libmemcached-dev \
musl \
postgresql-dev \
py-pip \
python3 \
python3-dev \
tmux \
vim \
wget \
zlib-dev \
&& rm -rf /var/cache/apk/*

# pip upgrades
RUN pip3 install --upgrade pip
RUN pip3 install -U "setuptools<46"
RUN apk update
RUN apk add sqlite-libs

RUN apk add \
    python3 \
    bash \
    nginx \
    uwsgi \
    uwsgi-python3 \
    supervisor && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    pip3 install 'pymysql<0.9' && \
    pip3 install wheel lxml && \
    pip3 install -r /tmp/requirements.txt && \
    rm /etc/nginx/conf.d/default.conf && \
    rm -r /root/.cache

# Copy the Nginx global conf
COPY nginx.conf /etc/nginx/
# Copy the Flask Nginx site conf
COPY flask-site-nginx.conf /etc/nginx/conf.d/
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY uwsgi.ini /etc/uwsgi/
# Custom Supervisord config
COPY supervisord.conf /etc/supervisord.conf

# Add demo app
COPY . /app
WORKDIR /app

# adding permission
RUN /bin/bash -c 'chmod 777 /app/app; chmod 777 /app/app/db.sqlite;' # add needed permission for DB access. Note: the folder access is needed as well as the file access

#start:
CMD ["/usr/bin/supervisord"]
