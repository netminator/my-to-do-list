# README #


### What is this repository for? ###

* Quick summary

This is a speedy summary ;)

This is basically an API endpoint, with very low - to now front end. The backend (API endpoint) will validate the data and perform CRUD.
  

* Version
  
0.1 far from finishing

### How do I get set up? ###

* Summary of set up

First get the [code](https://bitbucket.org/netminator/my-to-do-list/src/master/):

```git clone https://netminator@bitbucket.org/netminator/my-to-do-list.git```

There are two ways of setting this APP up:

1. Traditional Python 3 virtual env. Start the app via run.py
The requirements -> requirements.txt
   
By default, the app will load on localhost:5000 and will display the swagger doco:

![title](Images/swagger.png)

The below temporary front-end is located [here](http://127.0.0.1:5000/todo):

![title](Images/load.png)
   
2. Docker :

run the below cmd to build:

```docker build -t my_todo_app -f ./Dockerfile .```

and next to run:

```docker run --name mytodo -d -p 8080:80 my_todo_app:latest```

note: this will bind the app to your port 8080 - please ensure that the port is not allocated, alternatively please change it accordingly.

* Configuration
* Dependencies
 
Dependencies are listed in requirements.txt

* Database configuration
  
DB will load schema upon loading the app. There is a separate DB that loads records upon Unittest

* How to run tests

```python3 -m unittest unitests.py```

* Deployment instructions

### Backend ###

* API

The app is running almost only as an API endpoint.

Arguments:

name:* str(100)
description: str(500)
date: str (format: dd-mm-yyyy)
status:* bool (0 pending, 1 done)

####GET - will list all Todos :

endpoint:   ```http://127.0.0.1:5000/api/todos/```

```
import requests

url = "http://127.0.0.1:5000/api/todos/"

response = requests.request("GET", url)

```

####POST - creates a new service:

endpoint:   ```http://127.0.0.1:5000/api/todos/```

```
import requests

url = "http://127.0.0.1:5000/api/todos/"
payload="{\"description\": \"HxjpwcfPtzcAClyrvtUqOoIfDrlFfABbKenHOtXOXAA\", \"name\": \"between\", \"status\": 0}"
headers = {
  '': '',
  'Content-Type': 'application/json'
}
response = requests.request("POST", url, headers=headers, data=payload)

```

####PUT - update all fields of a To-do:

endpoint:   ```http://127.0.0.1:5000/api/todos/<todo_id>```

Arguments:

name:* str(100)
description:* str(500)
date:* str (format: dd-mm-yyyy)
status:* bool (0 pending, 1 done)

```
import requests

url = "http://127.0.0.1:5000/api/todos/3"

payload="{\"name\": \"off\", \"description\": \"\\u000b8v}7ey4 ;\\nn(7GQ7d!7+v}x!1yyF74;\\n\\\">..?_%<)+\\n#X_~O<A\\tYlGc64&BPQSqXVDaICBOT*K;o.=~BmvCdD\\u000bdy]('ZZ|$=p]sPjAbH\\\"~1;]+o~C%\\ng\\\\^\\\\IV>)_D`{'\\fDP)'KTod:STJrw79\\\\o_L\\\"}\\n}'GkNaM6 WwRb\\tlkX4!|o}\\tW>%B|-{PPQyh\\foQ\\\"a\\tn\\r\\f=VWd63DGc\\\\9<\\n ;l`^^Kx$|YEG7JH{om^KbX<`G%3zZn\\n$.*e!\\\\,T Vsm`'&qC.;~j)o.5.*bp.al7\\nub0B\\u000b\\\"@Q7w>~V}GadpWex9@!)nc\\nO\\r]jhS[>v0M;q20=7ZcmKH)On1`3|Hx_-|Gc{h\\nE+ B>\\u000b^=~T)#H-Pg}mY,P\\\\Nn+fx!y\\t;zE:\\u000b't4%~QL\\u000bfL1Q\\fOcR\", \"due_date\": \"09-10-2012\", \"status\": 1}"
headers = {
  '': '',
  'Content-Type': 'application/json'
}

response = requests.request("PUT", url, headers=headers, data=payload)
```

####PATCH - update one or all fields of a To-do:

endpoint:   ```http://127.0.0.1:5000/api/todos/<todo_id>```

Arguments:

name: str(100)
description: str(500)
date: str (format: dd-mm-yyyy)
status: bool (0 pending, 1 done)

```
url = "http://127.0.0.1:5000/api/todos/1"

payload="{\"description\": \"HxjpwcfPtzcAClyrvtUqOoIfDrlFfABbKenHOtXOXAA\", \"name\": \"between\", \"status\": 0}"
headers = {
  '': '',
  'Content-Type': 'application/json'
}

response = requests.request("PATCH", url, headers=headers, data=payload)
```


####GET - will list one Todo:

endpoint:   ```http://127.0.0.1:5000/api/todos/<todo_id>```

```
import requests

url = "http://127.0.0.1:5000/api/todos/"

response = requests.request("GET", url)

```

* Documentation

Swagger documentation is dynamic and almost all auto generated. There are a few items that needs attention (i.e. date format, missing descriptions)

