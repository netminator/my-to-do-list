from flask import Flask, Blueprint, url_for
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# /// = relative path, //// = absolute path
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite"
app.config["SECRET_KEY"] = "MySuperSEecret"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
api = Api(
    app,
    title="To-do list",
    description="To-do list API",
    default="Swagger Doco",
    default_label="Todo APP",
    ordered=True,
)


from app import routes
