from app import db, api
from app.models import Todo
from flask_restx import abort, Resource, reqparse
from flask import jsonify, make_response
from datetime import datetime


def abort_if_todo_doesnt_exist(todo_id):
    todo = Todo.query.filter_by(id=todo_id).first()
    if todo == None:
        abort(
            404,
            message=f"Sorry, to-do {todo_id} doesn't exist. Maybe try something else instead? ;)",
        )


def data_validation_len(test_string, max_string_length):
    if len(test_string) >= max_string_length:
        abort(
            400,
            message=f"Bad Request '{test_string}' has a max length of {max_string_length}",
        )


def data_validation_bool(test_bool):
    if type(test_bool) != bool:
        abort(400, message=f"Bad Request - not a Bool type")


def data_validation_date(date):
    try:
        due_date = datetime.strptime(str(date), "%d-%m-%Y")
    except ValueError as err:
        abort(
            400,
            message=f"Bad Request '{date}' does not seem to be a valid date. Please provide the date in the dd-mm-yyyy format",
        )
    return due_date


class ManipulateTodoAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "description", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "due_date", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "status", type=bool, location="json", store_missing=False
        )
        super(ManipulateTodoAPI, self).__init__()

    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        res = make_response(jsonify(todo), 200)
        return res

    def patch(self, todo_id):
        args = self.reqparse.parse_args()
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        # User can send here any number of arguments
        if "name" in args:
            data_validation_len(args["name"], 100)
            todo.name = args["name"]
        if "description" in args:
            data_validation_len(args["description"], 500)
            todo.description = args["description"]
        if "due_date" in args:
            todo.due_date = data_validation_date(args["due_date"])
        if "status" in args:
            data_validation_bool(args["status"])
            todo.status = args["status"]
        db.session.commit()
        res = make_response(jsonify(todo), 200)
        return res

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        print(f"Will delete todo {todo_id}")
        db.session.delete(todo)
        db.session.commit()
        return f"Todo ID:{todo_id} has been deleted", 204


class TodoListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name",
            type=str,
            location="json",
            required=True,
            help="No to- do name is provided",
        )
        self.reqparse.add_argument(
            "description", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "due_date", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "status",
            type=bool,
            location="json",
            required=True,
            help="Status of the to-do is required (true for done of false for pending)",
        )
        super(TodoListAPI, self).__init__()

    def get(self):
        todo_list = Todo.query.all()
        res = make_response(jsonify(todo_list), 200)
        return res

    def post(self):
        args = self.reqparse.parse_args()
        name = args["name"]
        data_validation_len(args["name"], 100)
        if "description" in args:
            data_validation_len(args["description"], 500)
            description = args["description"]
        else:
            description = None  # TODO probably there is more elegant way to validate this, will have to do some research
        if "due_date" in args:
            due_date = data_validation_date(args["due_date"])
        else:
            due_date = None
        status = args["status"]
        data_validation_bool(args["status"])
        new_todo = Todo(
            name=name, status=status, description=description, due_date=due_date
        )
        db.session.add(new_todo)
        db.session.commit()
        return {"Todo": "success"}, 201


class PutTodoAPI(Resource):
    """Put requires all values to be updated"""

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name",
            type=str,
            location="json",
            required=True,
            help="No to-do name is provided",
        )
        self.reqparse.add_argument(
            "description",
            type=str,
            location="json",
            required=True,
            help="No description is provided",
        )
        self.reqparse.add_argument(
            "due_date",
            type=str,
            location="json",
            required=True,
            help="No date is provided",
        )
        self.reqparse.add_argument(
            "status",
            type=bool,
            location="json",
            required=True,
            help="No status is provided",
        )
        super(PutTodoAPI, self).__init__()

    def put(self, todo_id):
        args = self.reqparse.parse_args()
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        todo.name = args["name"]
        todo.description = args["description"]
        todo.due_date = data_validation_date(args["due_date"])
        todo.status = args["status"]
        db.session.commit()
        res = make_response(jsonify(todo), 200)
        return res


class TestTodoAPI(Resource):
    def __init__(self):
        super(TestTodoAPI, self).__init__()

    def get(self):
        todo_list = Todo.query.order_by(Todo.id.desc()).first()
        todo_last_id = todo_list.id
        res = make_response(jsonify({"id": todo_last_id}), 200)
        return res
