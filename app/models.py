from app.routes import db, api
from app.routes import api
from dataclasses import dataclass
from marshmallow import ValidationError
from datetime import datetime
from flask_restx import fields

### Validators


def data_validation_date(date):
    try:
        due_date = datetime.strptime(str(date), "%d-%m-%Y")
    except ValueError as err:
        raise ValidationError(
            f"Bad Request '{date}' does not seem to be a valid date. Please provide the date in the dd-mm-yyyy format"
        )


@dataclass
class Todo(db.Model):

    id: int
    name: str
    description: str
    due_date: int
    status: bool

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(500))
    due_date = db.Column(db.Date)
    status = db.Column(db.Boolean, nullable=False)  # 0 for pending, 1 for done


patch_resource_fields = api.model(
    "Todo PATCH fields",
    {
        "name": fields.String(
            description="The Todo's name - max 100 char", max_length=100
        ),
        "description": fields.String(
            description="Short description - max 500 char", max_length=500
        ),
        "due_date": fields.Date,
        "status": fields.Boolean,
    },
)

post_resource_fields = api.model(
    "Todo POST fields",
    {
        "name": fields.String(
            description="The Todo's name - max 100 char", required=True, max_length=100
        ),
        "description": fields.String(
            description="Short description - max 500 char", max_length=500
        ),
        "due_date": fields.Date(
            description="Date in 'dd-mm-yyyy format"
        ),  # TODO According to doco, only yyy-mm-dd is supported: https://flask-restx.readthedocs.io/en/latest/swagger.html
        "status": fields.Boolean(required=True),
    },
)

put_resource_fields = api.model(
    "Todo PUT fields",
    {
        "name": fields.String(
            description="The Todo's name - max 100 char", required=True, max_length=100
        ),
        "description": fields.String(
            description="Short description - max 500 char",
            required=True,
            max_length=500,
        ),
        "due_date": fields.Date(dateformat="dd-mm-yyyy", required=True),
        "status": fields.Boolean(required=True, allow_none=False),
    },
)


########
db.create_all()
