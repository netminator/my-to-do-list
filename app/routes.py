from flask import render_template, request, redirect, url_for
from app import app, api, db
from app.models import (
    Todo,
    patch_resource_fields,
    put_resource_fields,
    post_resource_fields,
)
from pprint import pprint
from flask_restx import Resource, reqparse
from app.api.rest_todo import (
    abort_if_todo_doesnt_exist,
    data_validation_len,
    data_validation_bool,
    data_validation_date,
)
from flask import jsonify, make_response


@app.route("/todo")
def home():
    todo_list = Todo.query.all()
    pprint(todo_list)
    for twodo in todo_list:
        print(twodo, type(twodo))
        print(twodo.id)
    return render_template("base.html", todo_list=todo_list)


@app.route("/add", methods=["POST"])
def add():
    name = request.form.get("name")
    new_todo = Todo(name=name, status=False)
    db.session.add(new_todo)
    db.session.commit()
    return redirect(url_for("home"))


@app.route("/update/<int:todo_id>")
def update(todo_id):
    todo = Todo.query.filter_by(id=todo_id).first()
    # abort_if_todo_doesnt_exist
    todo.status = not todo.status
    db.session.commit()
    return redirect(url_for("home"))


@app.route("/delete/<int:todo_id>")
def delete(todo_id):
    todo = Todo.query.filter_by(id=todo_id).first()
    # abort_if_todo_doesnt_exist
    db.session.delete(todo)
    db.session.commit()
    return redirect(url_for("home"))


###################################


@api.route("/api/todos/<int:todo_id>")
class ManipulateTodoAPI(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        res = make_response(jsonify(todo), 200)
        return res

    @api.expect(patch_resource_fields)
    def patch(self, todo_id):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "description", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "due_date", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "status", type=bool, location="json", store_missing=False
        )

        args = self.reqparse.parse_args()
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        # User can send here any number of arguments
        if "name" in args:
            data_validation_len(args["name"], 100)
            todo.name = args["name"]
        if "description" in args:
            data_validation_len(args["description"], 500)
            todo.description = args["description"]
        if "due_date" in args:
            todo.due_date = data_validation_date(args["due_date"])
        if "status" in args:
            data_validation_bool(args["status"])
            todo.status = args["status"]
        db.session.commit()
        res = make_response(jsonify(todo), 200)
        return res

    @api.expect(put_resource_fields)
    def put(self, todo_id):

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name",
            type=str,
            location="json",
            required=True,
            help="No to-do name is provided",
        )
        self.reqparse.add_argument(
            "description",
            type=str,
            location="json",
            required=True,
            help="No description is provided",
        )
        self.reqparse.add_argument(
            "due_date",
            type=str,
            location="json",
            required=True,
            help="No date is provided",
        )
        self.reqparse.add_argument(
            "status",
            type=bool,
            location="json",
            required=True,
            help="No status is provided",
        )

        args = self.reqparse.parse_args()
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        todo.name = args["name"]
        todo.description = args["description"]
        todo.due_date = data_validation_date(args["due_date"])
        todo.status = args["status"]
        db.session.commit()
        res = make_response(jsonify(todo), 200)
        return res

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        todo = Todo.query.filter_by(id=todo_id).first()
        print(f"Will delete todo {todo_id}")
        db.session.delete(todo)
        db.session.commit()
        return f"Todo ID:{todo_id} has been deleted", 204


@api.route("/api/todos/")
class TodoListAPI(Resource):
    def get(self):
        todo_list = Todo.query.all()
        res = make_response(jsonify(todo_list), 200)
        return res

    @api.expect(post_resource_fields)
    def post(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "name",
            type=str,
            location="json",
            required=True,
            help="No to- do name is provided",
        )
        self.reqparse.add_argument(
            "description", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "due_date", type=str, location="json", store_missing=False
        )
        self.reqparse.add_argument(
            "status",
            type=bool,
            location="json",
            required=True,
            help="Status of the to-do is required (true for done of false for pending)",
        )
        args = self.reqparse.parse_args()
        name = args["name"]
        data_validation_len(args["name"], 100)
        if "description" in args:
            data_validation_len(args["description"], 500)
            description = args["description"]
        else:
            description = None  # TODO probably there is more elegant way to validate this, will have to do some research
        if "due_date" in args:
            due_date = data_validation_date(args["due_date"])
        else:
            due_date = None
        status = args["status"]
        data_validation_bool(args["status"])
        new_todo = Todo(
            name=name, status=status, description=description, due_date=due_date
        )
        db.session.add(new_todo)
        db.session.commit()
        return {"Todo": "Great success"}, 201
