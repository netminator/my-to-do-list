import itertools
from faker import Faker
import random
import string
from random import randrange
import json
from datetime import datetime

faker = Faker()


def text_generator(chars=string.printable):
    size = randrange(500)
    return "".join(random.choice(chars) for _ in range(size))


api_test_data = [
    {"name": f"{faker.word()}"},
    {"description": f"{text_generator()}"},
    {"due_date": f"{faker.date_this_century().strftime('%d-%m-%Y')}"},
    {"status": random.randint(0, 1)},
]

api_put_data = {
    "name": f"{faker.word()}",
    "description": f"{text_generator()}",
    "due_date": f"{faker.date_this_century().strftime('%d-%m-%Y')}",
    "status": random.randint(0, 1),
}


def create_test_data_for_test_patch_api(api_test_data):
    """this data is generated for testing patch API. I wanted this to be a bit more dynamic, so that you can change the api_test_data intput if needed"""

    list_of_possible_permutations = (
        []
    )  # this is to get all possible options for submitting data for patch

    for L in range(1, len(api_test_data) + 1):
        for subset in itertools.combinations(api_test_data, L):
            my_list_of_dictionaries = list(subset)
            result = {}
            for d in my_list_of_dictionaries:
                result.update(d)
            list_of_possible_permutations.append(result)

    return list_of_possible_permutations


def create_test_data_for_post_api(api_test_data):

    list_of_possible_permutations = (
        []
    )  # this is to get all possible options for submitting data for patch

    for L in range(1, len(api_test_data) + 1):  # only options with at least two options
        for subset in itertools.combinations(api_test_data, L):
            my_list_of_dictionaries = list(subset)
            result = {}
            for d in my_list_of_dictionaries:
                result.update(d)
                result.update(
                    {"name": f"{faker.word()}", "status": random.randint(0, 1)}
                )  # make sure that the required data is always there
            list_of_possible_permutations.append(result)

    # list_of_possible_permutations.append({"name": "test_data1","status": ""}) # adding a single record here
    # pprint(list_of_possible_permutations)

    for i in list_of_possible_permutations:
        print(i)

    return list_of_possible_permutations


api_patch_test_data = create_test_data_for_test_patch_api(api_test_data)
api_post_test_data = create_test_data_for_post_api(api_test_data)

missing_put_data_list = [
    {"to-do name": {"description": "bla", "due_date": "01-01-1970", "status": 0,}},
    {"description": {"name": "bla", "due_date": "01-01-1970", "status": 0}},
    {"date": {"name": "bla", "description": "bla", "status": 0}},
    {"status": {"name": "bla", "description": "bla", "due_date": "01-01-1970",}},
]
