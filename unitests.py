from app import app, db
import unittest
import json
from app.models import Todo
from app.test_data.testing_data_source import (
    api_patch_test_data,
    api_post_test_data,
    api_put_data,
    missing_put_data_list,
)


class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        """set the test env"""
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        # keep the testing DB separate from the "production"
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test_db.sqlite"
        db.create_all()
        test_record1 = Todo(
            name="test record1",
            description="my test description with a unique str:m88cxuttgdh8es866wrjnpprm ",
            status=False,
        )
        test_record2 = Todo(
            name="test record2", description="this one will be deleted", status=True
        )
        test_record3 = Todo(
            name="test record3", description="this one will be updated", status=True
        )
        test_record4 = Todo(
            name="test record4", description="this one will be patched", status=True
        )
        test_record5 = Todo(
            name="test record3", description="this one will be updated", status=True
        )
        db.session.add(test_record1)
        db.session.add(test_record2)
        db.session.add(test_record3)
        db.session.add(test_record4)
        db.session.add(test_record5)
        db.session.commit()

    def tearDown(self):
        """clean up"""
        db.drop_all()
        db.session.remove()

    # ensure that the swagger page loads
    def test_swagger_home(self):
        tester = app.test_client(self)
        response = tester.get("/", content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # ensure that the swagger page loads content
    def test_home_loads_content(self):
        tester = app.test_client(self)
        response = tester.get("/todo", content_type="html/text")
        self.assertTrue(b"Swagger Doco" in response.data)

    # ensure that the main page loads content
    def test_home_loads_content(self):
        tester = app.test_client(self)
        response = tester.get("/todo", content_type="html/text")
        self.assertTrue(b"To Do App" in response.data)

    # ensure that the home page loads
    def test_home(self):
        tester = app.test_client(self)
        response = tester.get("/", content_type="html/text")
        self.assertEqual(response.status_code, 200)

    ######################  API TESTING     ######################

    ###################### GET TESTING  ######################

    def test_get_all_api(self):
        url = "/api/todos/"
        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.get(url, headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_get_one_record_api(self):
        """This is to test if GET for a single to-do works"""
        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.get(f"/api/todos/1", headers=headers)
        last_post_description_raw = json.loads(
            response.data.decode("utf-8").replace("'", '"')
        )
        last_post_description = last_post_description_raw["description"]
        self.assertEqual(
            str(last_post_description),
            "my test description with a unique str:m88cxuttgdh8es866wrjnpprm ",
        )

    def test_get_one_record_api_that_not_exists(self):
        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.get(f"/api/todos/9999999", headers=headers)
        self.assertEqual(response.status_code, 404)

    def test_get_one_record_api_that_not_exists_check_message(self):
        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.get(f"/api/todos/9999999", headers=headers)
        self.assertTrue(
            b"doesn't exist. Maybe try something else instead? ;)" in response.data
        )

    ###################### PATCH TESTING  ######################

    def test_patch_api(self):

        for payload in api_patch_test_data:
            with self.subTest():
                url = "/api/todos/3"
                headers = {"Content-Type": "application/json"}
                self.app = app.test_client()
                response = self.app.patch(
                    url, headers=headers, data=json.dumps(payload)
                )
                self.assertEqual(response.status_code, 200)

    ###################### DELETE TESTING  ######################

    def test_delete_api(self):

        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.delete(f"/api/todos/2", headers=headers)
        self.assertEqual(response.status_code, 204)

    def test_delete_api_incorrect_todo_id(self):

        headers = {"Content-Type": "application/json"}
        self.app = app.test_client()
        response = self.app.delete(f"/api/todos/-2", headers=headers)
        self.assertEqual(response.status_code, 404)

    ###################### POST TESTING  ######################

    def test_post_api(self):

        for payload in api_post_test_data:
            with self.subTest():
                url = "/api/todos/"
                headers = {"Content-Type": "application/json"}
                self.app = app.test_client()
                response = self.app.post(url, headers=headers, data=json.dumps(payload))
                self.assertEqual(response.status_code, 201)

    ###################### PUT TESTING  ######################

    def test_put_api(self):
        headers = {"Content-Type": "application/json"}
        payload = api_put_data
        self.app = app.test_client()
        response = self.app.put(
            f"/api/todos/3", headers=headers, data=json.dumps(payload)
        )
        self.assertEqual(response.status_code, 200)

    def test_put_api_missing_json(self):
        for item in missing_put_data_list:
            for missing_data, incorrect_json in item.items():
                with self.subTest(missing_data=missing_data):
                    headers = {"Content-Type": "application/json"}
                    self.app = app.test_client()
                    response = self.app.put(
                        f"/api/todos/3",
                        headers=headers,
                        data=json.dumps(incorrect_json),
                    )
                    self.assertTrue(
                        b"No "
                        + bytes(str(missing_data), encoding="ascii")
                        + b" is provided"
                        in response.data
                    )

    # TODO validate incorrect input i.e. wrong data format. Similar tests should be for PATCH and POST


if __name__ == "__main__":
    unittest.main()
